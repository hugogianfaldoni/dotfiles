# i3 config

# Constants
set $mod Mod4
set $base00 #BEC0C4
set $base01 #878D95
set $base02 #666E79
set $base03 #47525F
set $base04 #28313C
set $base05 #B8525C
set $base06 #922E38
set $defgaps 10

# General config
 font pango:DejaVu Sans Mono 8
 floating_modifier $mod
 focus_follows_mouse yes
 #hide_edge_borders both
 new_window none
 new_float  none
 
# Windows keybindings
 bindsym $mod+Shift+q kill
  # focus
 bindsym $mod+Left  focus left
 bindsym $mod+Down  focus down
 bindsym $mod+Up    focus up
 bindsym $mod+Right focus right
 bindsym $mod+h     focus left
 bindsym $mod+j     focus down
 bindsym $mod+k     focus up
 bindsym $mod+l     focus right
  # moving window
 bindsym $mod+Shift+Left    move left
 bindsym $mod+Shift+Down    move down
 bindsym $mod+Shift+Up      move up
 bindsym $mod+Shift+Right   move right
# bindsym $mod+Control+Left  move left  100 px
# bindsym $mod+Control+Down  move down  100 px
# bindsym $mod+Control+Up    move up    100 px
# bindsym $mod+Control+Right move right 100 px
 bindsym $mod+Shift+h   move left
 bindsym $mod+Shift+j   move down
 bindsym $mod+Shift+k   move up
 bindsym $mod+Shift+l   move right
# bindsym $mod+Control+j move left  100 px
# bindsym $mod+Control+k move down  100 px
# bindsym $mod+Control+i move up    100 px
# bindsym $mod+Control+l move right 100 px

 bindsym $mod+Shift+v split h
 bindsym $mod+v       split v
 bindsym $mod+Shift+f fullscreen toggle
 bindsym $mod+Shift+space floating toggle
 bindsym $mod+space       focus mode_toggle
 mode "resize" {
  bindsym Left  resize shrink width  10 px or 10 ppt
  bindsym Down  resize grow   height 10 px or 10 ppt
  bindsym Up    resize shrink height 10 px or 10 ppt
  bindsym Right resize grow   width  10 px or 10 ppt
  bindsym Shift+Left  resize shrink width  1 px or 1 ppt
  bindsym Shift+Down  resize grow   height 1 px or 1 ppt
  bindsym Shift+Up    resize shrink height 1 px or 1 ppt
  bindsym Shift+Right resize grow   width  1 px or 1 ppt
  bindsym Escape mode "default"
  bindsym Return mode "default"
 }
 bindsym $mod+r mode "resize"

# Music control keybindings
 mode "music" {
  bindsym p exec --no-startup-id mpc toggle
  bindsym n exec --no-startup-id mpc next
  bindsym b exec --no-startup-id mpc prev
  bindsym s exec --no-startup-id mpc stop
  bindsym Escape mode "default"
  bindsym Return mode "default"
 }
 bindsym $mod+m mode "music"

# Program keybindings
 bindsym $mod+f            exec firefox
 bindsym $mod+d            exec rofi -show run
 bindsym $mod+Return       exec urxvt
 bindsym $mod+Shift+Return exec urxvt -name furxvt
 bindsym $mod+Delete       exec --no-startup-id ~/.config/i3/i3-exit

# Volume keybindings
 bindsym XF86AudioMute        exec --no-startup-id ~/.config/i3/volume-control mute
 bindsym XF86AudioLowerVolume exec --no-startup-id ~/.config/i3/volume-control minus
 bindsym XF86AudioRaiseVolume exec --no-startup-id ~/.config/i3/volume-control plus

# Backlight keybindings
 bindsym XF86MonBrightnessDown exec --no-startup-id light -U 5
 bindsym XF86MonBrightnessUp   exec --no-startup-id light -A 5

# Printscreen
# bindsym Print exec --no-startup-id scrot '/home/user/Pictures/%Y-%m-%d_%H:%M:%S.png'
 mode "screenshot" {
  bindsym Return exec --no-startup-id xfce4-screenshooter -s /home/user/Pictures -f ; mode "default"
  bindsym s exec --no-startup-id xfce4-screenshooter -s /home/user/Pictures -r ; mode "default"
  bindsym w exec --no-startup-id xfce4-screenshooter -s /home/user/Pictures -w ; mode "default"
  bindsym Escape mode "default"
 }
 bindsym Print mode "screenshot"

# Power keybindings
 bindsym XF86Sleep exec systemctl suspend

# Restart/Reload keybindings
 bindsym $mod+Shift+c reload
 bindsym $mod+Shift+r restart

# Workspace keybindings
 bindsym $mod+ampersand  workspace 1
 bindsym $mod+eacute     workspace 2
 bindsym $mod+quotedbl   workspace 3
 bindsym $mod+apostrophe workspace 4
 bindsym $mod+parenleft  workspace 5
 bindsym $mod+minus      workspace 6
 bindsym $mod+egrave     workspace 7
 bindsym $mod+underscore workspace 8
 bindsym $mod+ccedilla   workspace 9
 bindsym $mod+agrave     workspace 10
 bindsym $mod+Shift+ampersand  move container to workspace 1
 bindsym $mod+Shift+eacute     move container to workspace 2
 bindsym $mod+Shift+quotedbl   move container to workspace 3
 bindsym $mod+Shift+apostrophe move container to workspace 4
 bindsym $mod+Shift+5          move container to workspace 5
 bindsym $mod+Shift+minus      move container to workspace 6
 bindsym $mod+Shift+egrave     move container to workspace 7
 bindsym $mod+Shift+underscore move container to workspace 8
 bindsym $mod+Shift+ccedilla   move container to workspace 9
 bindsym $mod+Shift+agrave     move container to workspace 10
 bindsym $mod+p workspace prev
 bindsym $mod+n workspace next
# Gaps keybindings
set $mode_gaps Gaps: (o) outer, (i) inner
set $mode_gaps_outer Outer Gaps: p|m|r (local), Shift + p|m|r (global)
set $mode_gaps_inner Inner Gaps: p|m|r (local), Shift + p|m|r (global)
bindsym $mod+Shift+g mode "$mode_gaps"

mode "$mode_gaps" {
        bindsym o      mode "$mode_gaps_outer"
        bindsym i      mode "$mode_gaps_inner"
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

mode "$mode_gaps_inner" {
        bindsym p  gaps inner current plus 5
        bindsym m  gaps inner current minus 5
        bindsym r  gaps inner current set $defgaps

        bindsym Shift+p  gaps inner all plus 5
        bindsym Shift+m  gaps inner all minus 5
        bindsym Shift+r  gaps inner all set $defgaps

        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_outer" {
        bindsym p  gaps outer current plus 5
        bindsym m  gaps outer current minus 5
        bindsym r  gaps outer current set $defgaps

        bindsym Shift+p  gaps outer all plus 5
        bindsym Shift+m  gaps outer all minus 5
        bindsym Shift+r  gaps outer all set $defgaps

        bindsym Return mode "default"
        bindsym Escape mode "default"
}

# Gaps config
smart_gaps on
smart_borders no_gaps
gaps inner $defgaps

# Program specific configuration
 for_window [class="Wicd-client.py"]      floating enable
 for_window [class="GParted"]             floating enable
 for_window [class="Simec"]               floating enable
 for_window [class="Yad"]                 floating enable border none
 for_window [instance="furxvt"]           floating enable
 for_window [class="mpv"]                 floating enable border none, resize set 960 540, move absolute position center
 for_window [class="Argon"]               floating enable
 for_window [class="Keepassx2"]           floating enable, resize set 800 600
 for_window [class="Simplescreenrecorder"] floating enable, resize set 500 700
 for_window [window_role="gimp-dock"]     floating disable; move left; resize shrink width 50 px or 50ppt
 for_window [window_role="gimp-toolbox$"]  floating disable; move right; resize grow width 30 px or 30ppt
 #for_window [class="VirtualBox"]        floating enable
# Wallpaper keybindings
 bindsym $mod+w exec --no-startup-id feh --bg-fill -z --no-fehbg ~/Pictures/wallpapers/

# Bar
bar {
 font pango:FontAwesome 10, DejaVu Sans Mono 10
 position bottom
 status_command i3blocks -c ~/.config/i3/i3blocks.conf
 separator_symbol "〈"
 output eDP-1
 output HDMI-1
 colors {
  separator  $base00
  background $base04
  statusline #AAAAAA
#			border	background	text
  focused_workspace	$base02	$base02		$base00
  active_workspace	$base03	$base03		$base00
  inactive_workspace	$base03	$base03		$base00
  urgent_workspace	$base06	$base03		$base05
 }
}

# Starting programs
#exec_always --no-startup-id feh --bg-fill -z --no-fehbg ~/Pictures/wallpapers/
