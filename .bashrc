#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# If we're in tty, set color
if [ "$TERM" = "linux" ]; then
  echo -en "\\e]P0000000"
  echo -en "\\e]P19C3528"
  echo -en "\\e]P261BC3B"
  echo -en "\\e]P3F3B43A"
  echo -en "\\e]P40D68A8"
  echo -en "\\e]P5744560"
  echo -en "\\e]P6288E9C"
  echo -en "\\e]P7A2A2A2"

  clear
fi

record() {
    if [ -z $1 ]; then
      echo "Please give a path"
      return
    fi
    read -r X Y W H < <(slop -f '%x %y %w %h')
    ffmpeg -y -f x11grab -s $W"x"$H -threads 4 -i ":0.0+$X,$Y" -c:v libvpx -crf 4 -b:v 6M -an "$1"
}

capture() {
  if [ -z $1 ]; then
    echo "Please give a path"
    return
  fi
  read -r X Y W H < <(slop -f '%x %y %w %h')
  ffmpeg -s $W"x"$H -framerate 30 -f x11grab -i ":0.0+$X,$Y" -threads 4 -c:v libx264 -qp 20 -preset ultrafast "$1"
}

alias ls='ls --color=auto -h'
alias ll='ls -Al --group-directories-first'
alias cu='checkupdates'
alias neofetch='neofetch --block_range 0 15'
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'
alias vim='compton-trans -c 99 && vim'

nonzero_return() {
    RETVAL=$?
    [ $RETVAL -ne 0 ] && echo "($RETVAL)"
}

#PS1='[\u@\h \W]\$ '
#export PS1="\[\e[32m\]\u\[\e[m\]\[\e[32m\]@\[\e[m\]\[\e[32m\]\h\[\e[m\] \[\e[34m\]\w\[\e[m\]\\$ "
export PS1="┏\[\e[1;32m\][\u@\h]\[\e[m\] \[\e[1;34m\]\w\[\e[m\]\n┗\\[\e[1;31m\]\`nonzero_return\`\[\e[m\]\$ "
export VISUAL="vi"
export EDITOR="vi"
