set number
set relativenumber
set numberwidth=1
let g:syntastic_cpp_compiler_options = ' -std=c++17'
